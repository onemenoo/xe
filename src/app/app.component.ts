import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize, windowWhen } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {

  private searchLocation = new FormControl();
  private locations: any[];
  private places: string[] = [];
  private isLoading = false;
  private data: string = '';
  private textarea: string = '';
  private language = (navigator.languages.find(element => element == 'el' || element == 'en')); //identify language from browser
  constructor(private http: HttpClient,
    private breakpointObserver: BreakpointObserver) {

    this.searchLocation.valueChanges
      .pipe(
        debounceTime(1000),//execute after 1 sec when end typing.. 
        tap(() => {//on taping, reset the location list and loading message
          this.locations = [];
          this.isLoading = true;
        }),
        switchMap(value => {
          //cansel the previous interval and http request and create new one
          if (!value || !this.language) {
            this.locations = [];
            this.isLoading = false
            return []
          }
          return this.http.get("http://35.180.182.8/search?keywords=" + value + "&language=" + this.language)
            .pipe(
              finalize(() => {
                //remove the loading
                this.isLoading = false
              }),
            )
        }
        )
      )
      .subscribe((data: any) => {
        if (data.entries == undefined) {
          this.locations = [];
        } else {
          let max = 5;
          for (let i = 0; i < data.entries.length; i++) {
            this.locations.push(data.entries[i]);

            if (i == (max - 1)) {
              break;
            }
          }
        }
      })
  }
  addLocation(data: string) {
    this.places.push(data);
    this.textarea = this.places.join('\n')
  }
  onSearch() {
    window.location.href = 'https://www.google.com/search?q=' + this.data;
  }
}
